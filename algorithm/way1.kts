fun findMin(arr: IntArray): Int {
    var min: Int = arr[0]
    for (id: Int in 1 until arr.size) {
        if (min > arr[id]) min = arr[id]
    }
    return min;
}
fun findMax(arr: IntArray): Int {
    var max: Int = arr[0]
    for (id: Int in 1 until arr.size) {
        if (max < arr[id]) max = arr[id]
    }
    return max;
}
fun miniMaxSum(arr: IntArray) {
    val min = findMin(arr);
    val max = findMax(arr);

    var sum: Long = 0;

    for (item: Int in arr) {
        sum += item;
    }
    var minSum = sum - max;
    var maxSum = sum - min;

    println("Output: $minSum $maxSum")
}

fun main() {
    print("Enter array input: ")
    val stringInput = readLine()!!
    val arr = stringInput.split(' ').map { it.toInt() }.toIntArray()

    miniMaxSum(arr)
}

main()