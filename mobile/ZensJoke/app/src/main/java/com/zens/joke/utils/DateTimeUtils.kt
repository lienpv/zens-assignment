package com.zens.joke.utils

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtils {
    private const val timeMillis = 1000

    val currentTimeMillis: Long
        get() = System.currentTimeMillis()

    val currentTime: String
        get() = (currentTimeMillis / timeMillis).toString()

    val today: String
        get() = SimpleDateFormat("yyyy-MM-dd").format(Date())

    val yesterday: String
        get() {
            val cal = Calendar.getInstance()
            cal.add(Calendar.DATE, -1)
            return SimpleDateFormat("yyyy-MM-dd").format(cal.time)
        }

    val timezone: String
        get() {
            val calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"), Locale.getDefault())
            val currentLocalTime = calendar.time
            val date: DateFormat = SimpleDateFormat("Z")
            return date.format(currentLocalTime)
        }
}