package com.zens.joke.utils

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.annotation.Keep
import java.security.MessageDigest

@Keep
object AppUtils {
    @JvmStatic
    fun getAppName(aContext: Context): String {
        var appName = ""
        try {
            val manager = aContext.packageManager
            val info = manager.getPackageInfo(aContext.packageName, 0)
            appName = aContext.resources.getString(info.applicationInfo.labelRes)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return appName
    }

    @JvmStatic
    fun getAppVersionName(aContext: Context): String {
        return try {
            val pInfo = aContext.packageManager.getPackageInfo(
                aContext.packageName,
                PackageManager.GET_META_DATA
            )
            pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            ""
        }
    }

    @JvmStatic
    fun getAppVersionCode(aContext: Context): Int {
        return try {
            val pInfo = aContext.packageManager.getPackageInfo(
                aContext.packageName,
                PackageManager.GET_META_DATA
            )
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) pInfo.longVersionCode.toInt() else pInfo.versionCode
        } catch (e: PackageManager.NameNotFoundException) {
            0
        }
    }

    @JvmStatic
    fun getAppSignature(context: Context): String {
        val signatureList: List<String>
        val packageName: String = context.packageName
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                // New signature
                val sig = context.packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNING_CERTIFICATES).signingInfo
                signatureList = if (sig.hasMultipleSigners()) {
                    // Send all with apkContentsSigners
                    sig.apkContentsSigners.map {
                        val digest = MessageDigest.getInstance("SHA")
                        digest.update(it.toByteArray())
                        bytesToHex(digest.digest())
                    }
                } else {
                    // Send one with signingCertificateHistory
                    sig.signingCertificateHistory.map {
                        val digest = MessageDigest.getInstance("SHA")
                        digest.update(it.toByteArray())
                        bytesToHex(digest.digest())
                    }
                }
            } else {
                val sig = context.packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES).signatures
                signatureList = sig.map {
                    val digest = MessageDigest.getInstance("SHA")
                    digest.update(it.toByteArray())
                    bytesToHex(digest.digest())
                }
            }

            return signatureList[0]
        } catch (e: Exception) {
            // Handle error
        }
        return String()
    }

    private fun bytesToHex(bytes: ByteArray): String {
        val hexArray = charArrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F')
        val hexChars = CharArray(bytes.size * 2)
        var v: Int
        for (j in bytes.indices) {
            v = bytes[j].toInt() and 0xFF
            hexChars[j * 2] = hexArray[v.ushr(4)]
            hexChars[j * 2 + 1] = hexArray[v and 0x0F]
        }
        return String(hexChars)
    }
}