package com.zens.joke.core.app

import android.app.Application
import com.zens.joke.utils.PrefsUtils

class JokeApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        instance = this

        PrefsUtils.initPrefsIfNeed(this)
    }

    companion object {
        var instance: JokeApplication? = null

        fun get(): JokeApplication? {
            return instance
        }
    }
}