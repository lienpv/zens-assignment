package com.zens.joke.database.repository

import android.app.Application
import androidx.lifecycle.LiveData
import com.zens.joke.database.JokeDatabase
import com.zens.joke.database.dao.JokeDao
import com.zens.joke.database.model.JokeModel

class JokeRepository(application: Application) {
    private val jokeDao: JokeDao = JokeDatabase(application).jokeDao()

    fun getAllJoke(): LiveData<List<JokeModel>> = jokeDao.getAllJoke()
    fun getRandomJoke(limit: Int): LiveData<List<JokeModel>> = jokeDao.getRandomJoke(limit)
    suspend fun addJoke(jokeModel: JokeModel) = jokeDao.addJoke(jokeModel)
    suspend fun updateJoke(jokeModel: JokeModel) = jokeDao.updateJoke(jokeModel)
    suspend fun deleteJoke(jokeModel: JokeModel) = jokeDao.deleteJoke(jokeModel)
    suspend fun deleteAllJokes(jokeList: List<JokeModel>) = jokeDao.deleteAllJokes(jokeList)
    suspend fun like(jId: Int) = jokeDao.like(jId)
    suspend fun dislike(jId: Int) = jokeDao.dislike(jId)
}