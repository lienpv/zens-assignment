package com.zens.joke.database.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.zens.joke.utils.DateTimeUtils
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "tbJoke")
data class JokeModel(
    @PrimaryKey(autoGenerate = true) val id: Int,
    val title: String,
    val subtitle: String,
    val content: String,
    val vote: Boolean,
    val read: Boolean
): Parcelable {
    var createAt: Long = DateTimeUtils.currentTimeMillis
}