package com.zens.joke.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.zens.joke.database.model.JokeModel

@Dao
interface JokeDao {
    @Query("SELECT * FROM tbJoke")
    fun getAllJoke(): LiveData<List<JokeModel>>
    @Query("SELECT * FROM tbJoke WHERE read=false ORDER BY RANDOM() LIMIT :limit")
    fun getRandomJoke(limit: Int): LiveData<List<JokeModel>>
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addJoke(feedModel: JokeModel)
    @Update
    suspend fun updateJoke(feedModel: JokeModel)
    @Delete
    suspend fun deleteJoke(feedModel: JokeModel)
    @Delete
    suspend fun deleteAllJokes(feedList: List<JokeModel>)
    @Query("UPDATE tbJoke SET vote=true, read=true WHERE id=:jId")
    suspend fun like(jId: Int)
    @Query("UPDATE tbJoke SET vote=false, read=true WHERE id=:jId")
    suspend fun dislike(jId: Int)
}