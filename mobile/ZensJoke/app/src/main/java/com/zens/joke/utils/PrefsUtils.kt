package com.zens.joke.utils

import android.content.Context
import android.content.ContextWrapper
import android.content.SharedPreferences
import android.text.TextUtils
import org.json.JSONArray
import org.json.JSONException
import java.util.*

object PrefsUtils {
    private const val DEFAULT_SUFFIX = "_preferences"
    private var mPrefs: SharedPreferences? = null
    private fun initPrefs(context: Context, prefsName: String?, mode: Int) {
        mPrefs = context.getSharedPreferences(prefsName, mode)
    }

    fun initPrefsIfNeed(aContext: Context) {
        if (mPrefs == null) {
            Builder()
                    .setContext(aContext)
                    .setMode(ContextWrapper.MODE_PRIVATE)
                    .setPrefsName(aContext.packageName)
                    .setUseDefaultSharedPreference(true)
                    .build()
        }
    }

    private val preferences: SharedPreferences?
        get() = if (mPrefs != null) {
            mPrefs
        } else null

    fun getInt(key: String?, defValue: Int): Int {
        return preferences!!.getInt(key, defValue)
    }

    fun getInt(key: String?): Int {
        return preferences!!.getInt(key, 0)
    }

    fun getBoolean(key: String?, defValue: Boolean): Boolean {
        return preferences!!.getBoolean(key, defValue)
    }

    fun getBoolean(key: String?): Boolean {
        return preferences!!.getBoolean(key, false)
    }

    fun getLong(key: String?, defValue: Long): Long {
        return preferences!!.getLong(key, defValue)
    }

    fun getLong(key: String?): Long {
        return preferences!!.getLong(key, 0L)
    }

    fun getDouble(key: String?, defValue: Double): Double {
        return java.lang.Double.longBitsToDouble(preferences!!.getLong(key, java.lang.Double.doubleToLongBits(defValue)))
    }

    fun getDouble(key: String?): Double {
        return java.lang.Double.longBitsToDouble(preferences!!.getLong(key, java.lang.Double.doubleToLongBits(0.0)))
    }

    fun getFloat(key: String?, defValue: Float): Float {
        return preferences!!.getFloat(key, defValue)
    }

    fun getFloat(key: String?): Float {
        return preferences!!.getFloat(key, 0.0f)
    }

    fun getString(key: String?, defValue: String?): String? {
        return preferences!!.getString(key, defValue)
    }

    fun getString(key: String?): String? {
        return preferences!!.getString(key, "")
    }

    fun putLong(key: String?, value: Long) {
        val editor = preferences!!.edit()
        editor.putLong(key, value)
        editor.apply()
    }

    fun putInt(key: String?, value: Int) {
        val editor = preferences!!.edit()
        editor.putInt(key, value)
        editor.apply()
    }

    fun putBoolean(key: String?, value: Boolean) {
        val editor = preferences!!.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun putDouble(key: String?, value: Double) {
        val editor = preferences!!.edit()
        editor.putLong(key, java.lang.Double.doubleToRawLongBits(value))
        editor.apply()
    }

    fun putFloat(key: String?, value: Float) {
        val editor = preferences!!.edit()
        editor.putFloat(key, value)
        editor.apply()
    }

    fun putString(key: String?, value: String?) {
        val editor = preferences!!.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun putJSONArray(key: String?, value: JSONArray) {
        putString(key, value.toString())
    }

    fun remove(key: String) {
        val prefs = preferences
        val editor = prefs!!.edit()
        if (prefs.contains(key)) {
            editor.remove(key)
            editor.apply()
        }
    }

    fun clear(): SharedPreferences.Editor {
        val editor = preferences!!.edit().clear()
        editor.apply()
        return editor
    }

    fun edit(): SharedPreferences.Editor {
        return preferences!!.edit()
    }

    operator fun contains(key: String?): Boolean {
        return preferences!!.contains(key)
    }

    class Builder {
        private var mKey: String? = null
        private var mContext: Context? = null
        private var mMode = -1
        private var mUseDefault = false
        fun setPrefsName(prefsName: String?): Builder {
            mKey = prefsName
            return this
        }

        fun setContext(context: Context?): Builder {
            mContext = context
            return this
        }

        fun setMode(mode: Int): Builder {
            mMode = if (mode == ContextWrapper.MODE_PRIVATE) {
                mode
            } else {
                throw RuntimeException("The mode in the SharedPreference can only be set too ContextWrapper.MODE_PRIVATE, ContextWrapper.MODE_WORLD_READABLE, ContextWrapper.MODE_WORLD_WRITEABLE or ContextWrapper.MODE_MULTI_PROCESS")
            }
            return this
        }

        fun setUseDefaultSharedPreference(defaultSharedPreference: Boolean): Builder {
            mUseDefault = defaultSharedPreference
            return this
        }

        fun build() {
            if (mContext == null) {
                throw RuntimeException("Context not set, please set context before building the Prefs instance.")
            }
            if (TextUtils.isEmpty(mKey)) {
                mKey = mContext!!.packageName
            }
            if (mUseDefault) {
                mKey += DEFAULT_SUFFIX
            }
            if (mMode == -1) {
                mMode = ContextWrapper.MODE_PRIVATE
            }
            initPrefs(mContext!!, mKey, mMode)
        }
    }
}