package com.zens.joke.database.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.CreationExtras
import com.zens.joke.database.model.JokeModel
import com.zens.joke.database.repository.JokeRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class JokeViewModel(application: Application): ViewModel() {
    private val jokeRepository: JokeRepository = JokeRepository(application)

    fun getAllJoke(): LiveData<List<JokeModel>> = jokeRepository.getAllJoke()
    fun getRandomJoke(limit: Int): LiveData<List<JokeModel>> = jokeRepository.getRandomJoke(limit)
    fun addJoke(jokeModel: JokeModel) = viewModelScope.launch {
        jokeRepository.addJoke(jokeModel)
    }
    fun updateJoke(jokeModel: JokeModel) = viewModelScope.launch {
        jokeRepository.updateJoke(jokeModel)
    }
    fun deleteJoke(jokeModel: JokeModel) = viewModelScope.launch {
        jokeRepository.deleteJoke(jokeModel)
    }
    fun deleteAllJokes(jokeList: List<JokeModel>) = viewModelScope.launch {
        jokeRepository.deleteAllJokes(jokeList)
    }
    fun like(jId: Int) = viewModelScope.launch {
        jokeRepository.like(jId)
    }
    fun dislike(jId: Int) = viewModelScope.launch {
        jokeRepository.dislike(jId)
    }

    class JokeViewModelFactory(private val application: Application): ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T {
            if (modelClass.isAssignableFrom(JokeViewModel::class.java)) {
                return JokeViewModel(application) as T
            }

            throw IllegalArgumentException("Unable construct JokeViewModel")
        }

    }
}