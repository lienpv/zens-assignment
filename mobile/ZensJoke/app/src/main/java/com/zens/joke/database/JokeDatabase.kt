package com.zens.joke.database

import android.content.Context
import androidx.lifecycle.viewModelScope
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.zens.joke.database.dao.JokeDao
import com.zens.joke.database.model.JokeModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Database(entities = [JokeModel::class], version = 1)
abstract class JokeDatabase: RoomDatabase() {
    abstract fun jokeDao(): JokeDao

    companion object {
        private val dbCallback = object :RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                CoroutineScope(Dispatchers.IO).launch {
                    initData()
                }

                super.onCreate(db)
            }
        }

        private suspend fun initData() {
            val dao = get()?.jokeDao()
            dao?.addJoke(
                JokeModel(
                1,
                "1-A joke a day keeps the doctor away",
                "1-If you joke wrong way, your teeth have to pay. (Serious)",
                "1-A child asked his father, \"How were people born?\" So his father said, \"Adam and Eve made babies, then their babies became adults anda made babies, and so on.\" The child then went to his mother, asked her the same question and she told him, \"We were mon-keys then we evolved to become like we are now.\"\n" +
                        "The child ran back to his father and said, \"You lied to me!\" His father replied, \"No, your mom was talking about her side of the family!\"",
                    vote = false,
                    read = false,
                )
            )
            dao?.addJoke(
                JokeModel(
                2,
                "2-A joke a day keeps the doctor away",
                "2-If you joke wrong way, your teeth have to pay. (Serious)",
                "2-A child asked his father, \"How were people born?\" So his father said, \"Adam and Eve made babies, then their babies became adults anda made babies, and so on.\" The child then went to his mother, asked her the same question and she told him, \"We were mon-keys then we evolved to become like we are now.\"\n" +
                        "The child ran back to his father and said, \"You lied to me!\" His father replied, \"No, your mom was talking about her side of the family!\"",
                    vote = false,
                    read = false,
                )
            )
            dao?.addJoke(
                JokeModel(
                3,
                "3-A joke a day keeps the doctor away",
                "3-If you joke wrong way, your teeth have to pay. (Serious)",
                "3-A child asked his father, \"How were people born?\" So his father said, \"Adam and Eve made babies, then their babies became adults anda made babies, and so on.\" The child then went to his mother, asked her the same question and she told him, \"We were mon-keys then we evolved to become like we are now.\"\n" +
                        "The child ran back to his father and said, \"You lied to me!\" His father replied, \"No, your mom was talking about her side of the family!\"",
                    vote = false,
                    read = false,
                )
            )
            dao?.addJoke(
                JokeModel(
                4,
                "4-A joke a day keeps the doctor away",
                "4-If you joke wrong way, your teeth have to pay. (Serious)",
                "4-A child asked his father, \"How were people born?\" So his father said, \"Adam and Eve made babies, then their babies became adults anda made babies, and so on.\" The child then went to his mother, asked her the same question and she told him, \"We were mon-keys then we evolved to become like we are now.\"\n" +
                        "The child ran back to his father and said, \"You lied to me!\" His father replied, \"No, your mom was talking about her side of the family!\"",
                    vote = false,
                    read = false,
                )
            )
            dao?.addJoke(
                JokeModel(
                5,
                "5-A joke a day keeps the doctor away",
                "5-If you joke wrong way, your teeth have to pay. (Serious)",
                "5-A child asked his father, \"How were people born?\" So his father said, \"Adam and Eve made babies, then their babies became adults anda made babies, and so on.\" The child then went to his mother, asked her the same question and she told him, \"We were mon-keys then we evolved to become like we are now.\"\n" +
                        "The child ran back to his father and said, \"You lied to me!\" His father replied, \"No, your mom was talking about her side of the family!\"",
                    vote = false,
                    read = false,
                )
            )
            dao?.addJoke(
                JokeModel(
                6,
                "6-A joke a day keeps the doctor away",
                "6-If you joke wrong way, your teeth have to pay. (Serious)",
                "6-A child asked his father, \"How were people born?\" So his father said, \"Adam and Eve made babies, then their babies became adults anda made babies, and so on.\" The child then went to his mother, asked her the same question and she told him, \"We were mon-keys then we evolved to become like we are now.\"\n" +
                        "The child ran back to his father and said, \"You lied to me!\" His father replied, \"No, your mom was talking about her side of the family!\"",
                    vote = false,
                    read = false,
                )
            )
        }

        @Volatile private var instance: JokeDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context)= instance ?: synchronized(LOCK){
            instance ?: buildDatabase(context).also { instance = it}
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(context,
                JokeDatabase::class.java, "dbZensJoke.db")
                .addCallback(dbCallback)
                .build()

        @JvmStatic
        fun get(): JokeDatabase? {
            return instance
        }
    }
}