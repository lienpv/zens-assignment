package com.zens.joke.core.base

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

open class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun gotoActivity(activity: AppCompatActivity?, aClass: Class<*>?) {
        val iGoto = Intent(activity, aClass)
        startActivity(iGoto)
    }
    fun gotoActivity(activity: AppCompatActivity?, aClass: Class<*>?, bundle: Bundle?) {
        val iGoto = Intent(activity, aClass)
        iGoto.putExtras(bundle!!)
        startActivity(iGoto)
    }
}