package com.zens.joke.core.app

object AppConstants {
    const val PREFS_JOKE_VOTE = "PREFS_JOKE_VOTE"
    const val PREFS_JOKE_VOTE_DATETIME = "PREFS_JOKE_VOTE_DATETIME"
}