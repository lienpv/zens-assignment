package com.zens.joke.ui.home

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.zens.joke.core.app.AppConstants
import com.zens.joke.database.model.JokeModel
import com.zens.joke.database.viewmodel.JokeViewModel
import com.zens.joke.databinding.ActivityMainBinding
import com.zens.joke.utils.DateTimeUtils
import com.zens.joke.utils.PrefsUtils
import java.util.*
import kotlin.concurrent.schedule

class MainActivity : AppCompatActivity() {
    private val jokeViewModel: JokeViewModel by lazy {
        ViewModelProvider(this, JokeViewModel.JokeViewModelFactory(application))[JokeViewModel::class.java]
    }

    private val jokeList: ArrayList<JokeModel> = ArrayList()
    private var limit: Int = 3
    private var currentJokeIndex: Int = 0
    private var todayVote: Int = 0

    private lateinit var binding: ActivityMainBinding
    private lateinit var liveData: LiveData<List<JokeModel>>

    private var jokeObserver: Observer<List<JokeModel>> =  Observer<List<JokeModel>> { list ->
        jokeList.clear()
        jokeList.addAll(list)

        binding.rlLoading.visibility = View.INVISIBLE

        if (jokeList.isNotEmpty()) {
            setupData(jokeList[currentJokeIndex])
            removeObserver()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val trackingDateTime: Long = PrefsUtils.getLong(AppConstants.PREFS_JOKE_VOTE_DATETIME, 0)
        if (DateTimeUtils.currentTimeMillis - trackingDateTime < 24*60*60*1000) {
            todayVote = PrefsUtils.getInt(AppConstants.PREFS_JOKE_VOTE, 0)
        }
        else {
            todayVote = 0

            PrefsUtils.putLong(AppConstants.PREFS_JOKE_VOTE_DATETIME, DateTimeUtils.currentTimeMillis)
            PrefsUtils.putInt(AppConstants.PREFS_JOKE_VOTE, 0)
        }
        limit -= todayVote

        if (limit > 0) {
            liveData = jokeViewModel.getRandomJoke(limit)
            liveData.observe(this, jokeObserver)
        }
        else {
            binding.rlLoading.visibility = View.INVISIBLE

            Toast.makeText(this, "That's all the jokes for today! Come back another day!", Toast.LENGTH_SHORT).show()
        }

        binding.btnLike.setOnClickListener {
            if (currentJokeIndex < jokeList.size) {
                jokeViewModel.like(jokeList[currentJokeIndex].id)
            }

            //Next Joke
            nextJoke()
        }
        binding.btnDislike.setOnClickListener {
            if (currentJokeIndex < jokeList.size) {
                jokeViewModel.dislike(jokeList[currentJokeIndex].id)
            }

            //Next Joke
            nextJoke()
        }
        binding.rlLoading.setOnClickListener {  }
    }

    private fun removeObserver() {
        if (liveData.hasActiveObservers()) {
            liveData.removeObserver(jokeObserver)
        }
    }
    private fun setupData(jokeModel: JokeModel) {
        binding.tvTitle.text = jokeModel.title
        binding.tvSubtitle.text = jokeModel.subtitle
        binding.tvContent.text = jokeModel.content
    }
    private fun nextJoke() {
        if (currentJokeIndex <= jokeList.size - 1) {
            binding.rlLoading.visibility = View.VISIBLE

            Timer().schedule(1000){
                runOnUiThread {
                    binding.rlLoading.visibility = View.INVISIBLE

                    todayVote = PrefsUtils.getInt(AppConstants.PREFS_JOKE_VOTE, 0) + 1
                    PrefsUtils.putInt(AppConstants.PREFS_JOKE_VOTE, todayVote)

                    currentJokeIndex++
                    if (currentJokeIndex < jokeList.size) {
                        setupData(jokeList[currentJokeIndex])
                    }
                }
            }
        }
        else {
            Toast.makeText(this, "That's all the jokes for today! Come back another day!", Toast.LENGTH_SHORT).show()
        }
    }
}