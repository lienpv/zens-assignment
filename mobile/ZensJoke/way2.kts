fun miniMaxSum(arr: IntArray) {
    var sum: Long = 0
    var min: Int = arr[0]
    var max: Int = arr[0]

    for (item: Int in arr) {
        sum += item;
        if (min > item) min = item
        if (max < item) max = item
    }
    var minSum = sum - max;
    var maxSum = sum - min;

    println("Output: $minSum $maxSum")
}
fun main() {
    print("Enter array input: ")
    val stringInput = readLine()!!
    val arr = stringInput.split(' ').map { it.toInt() }.toIntArray()

    miniMaxSum(arr)
}

main()